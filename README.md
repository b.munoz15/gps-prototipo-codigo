# Proyecto de Obtención y Envío de Datos de Ubicación a ThingsBoard

Este proyecto es un ejemplo de cómo obtener datos de ubicación de un módulo GPS y enviarlos a un servidor ThingsBoard utilizando MQTT. El código está diseñado para funcionar en un dispositivo compatible con el módulo WiFi101, como el Arduino MKR1000.

## Requisitos
* Dispositivo compatible con el módulo WiFi101, como Arduino MKR1000.
* Módulo GPS conectado al dispositivo a través del puerto serie.
* Acceso a una red WiFi con las credenciales proporcionadas en el código.
* Servidor ThingsBoard configurado y accesible.
## Configuración
1. Conecta el módulo GPS al dispositivo compatible y asegúrate de que esté configurado correctamente para enviar datos válidos.

2. bre el archivo de código fuente main.ino en tu entorno de desarrollo para dispositivos Arduino.

3. En el archivo main.ino, encuentra la sección "Configuración WiFi" y reemplaza los valores de ssid y pass con las credenciales de tu red WiFi.

4. En la sección "Configuración ThingsBoard", reemplaza los valores de TOKEN, THINGSBOARD_SERVER y THINGSBOARD_PORT con los valores correspondientes de tu servidor ThingsBoard.

5. Verifica que la velocidad de baudios en SERIAL_DEBUG_BAUD coincida con la configuración de tu monitor serie.

6. Sube el código al dispositivo compatible y verifica que no haya errores de compilación.

## Prueba
1. Conecta el dispositivo a una fuente de alimentación.

2. Abre el monitor serie en tu entorno de desarrollo para dispositivos Arduino.

3. Observa los mensajes en el monitor serie para verificar la conexión WiFi. Deberías ver mensajes que indiquen la conexión exitosa a la red WiFi.

4. Una vez establecida la conexión WiFi, el dispositivo intentará conectarse al servidor ThingsBoard utilizando MQTT. Verifica los mensajes en el monitor serie para asegurarte de que la conexión MQTT se establezca correctamente.

5. Si la conexión MQTT se establece correctamente, el dispositivo enviará comandos al módulo GPS para obtener la información de ubicación. Los datos de ubicación (latitud, longitud y velocidad) se imprimirán en el monitor serie y se enviarán al servidor ThingsBoard.

6. Verifica los mensajes en el monitor serie para verificar si la comunicación con el módulo GPS y el servidor ThingsBoard es exitosa. Además, verifica en el panel de control de ThingsBoard si los datos de ubicación se reciben correctamente.

## Notas
1. Asegúrate de tener una conexión WiFi estable y acceso válido al servidor ThingsBoard antes de probar el código.
2. Si encuentras problemas de conexión, verifica tus conexiones físicas, credenciales de red WiFi y configuración del servidor ThingsBoard.