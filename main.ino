#include <PubSubClient.h>
#include <WiFi101.h>

//acceso wifi
char ssid[] = "bpmh";
char pass[] = "202528783";

//acceso ThingsBoard
#define TOKEN "rmUvTwVVwBa87WBCMwG1"
#define THINGSBOARD_SERVER "iot.ceisufro.cl"
#define THINGSBOARD_PORT 1883

#define SERIAL_DEBUG_BAUD 9600

//wifi cliente
WiFiClient client;

//iniciar cliente mqtt
PubSubClient mqttClient(client);


bool modemConnected = false;
String response = "";

//setup
void setup() {
  Serial.println("-------SETUP------");
  //iniciar modulo gps y mrk1000
  Serial.begin(SERIAL_DEBUG_BAUD);
  Serial1.begin(SERIAL_DEBUG_BAUD);

  while (!Serial) {
    ;  // Esperar a que el Monitor Serie se abra
  }
  //iniciar gps
  Serial1.println("AT+CGNSPWR=1");
  Serial1.println("AT+CGPSPWR=1");


  //conexión WiFi
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    WiFi.begin(ssid, pass);  // Intentar reconexión
    delay(1000);
    Serial.println("Conectando a WiFi...");
  }
  Serial.println("Conexión WiFi establecida");

  //inicializar servidor mqtt
  mqttClient.setServer(THINGSBOARD_SERVER, THINGSBOARD_PORT);
}


void loop() {
  // Verificar si la conexión WiFi está activa
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Conexión WiFi perdida. Intentando reconectar...");
    while (WiFi.status() != WL_CONNECTED) {
      WiFi.begin(ssid, pass);  // Intentar reconexión
      delay(1000);
    }
    Serial.println("Conexión WiFi establecida");
  }

  if (!mqttClient.connected()) {
    reconnectToMqtt();
  }

  mqttClient.loop();
  //enviar comando para obtener informacion ubicacion
  Serial1.println("AT+CGNSINF");
  // Esperar a que llegue la respuesta del SerialGPS
  while (!Serial1.available()) {
    ;  // Esperar
  }
  // Leer la respuesta del SerialGPS
  while (Serial1.available()) {
    char data = Serial1.read();
    response += data;
  }
  Serial.println(response);

  float location[3];

  parseCGNSINF(response, location);
  // Imprimir la latitud y la longitud
  Serial.print("Latitud: ");
  Serial.println(location[0], 6);
  Serial.print("Longitud: ");
  Serial.println(location[1], 6);
  Serial.print("Velocidad: ");
  Serial.println(location[2], 6);

  Serial.println("Sending latitude data...");
  publishTelemetry("latitud", convertFloatToChar(location[0]));

  Serial.println("Sending longitude data...");
  publishTelemetry("longitud", convertFloatToChar(location[1]));
  Serial.println("Sending velocity data...");
  publishTelemetry("velocidad", convertFloatToChar(location[2]));
  response = "";
  delay(10000);
}
void reconnectToMqtt() {
  while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (mqttClient.connect("clientId", TOKEN,NULL)) {
      Serial.println("connected");
      modemConnected = true;
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" retrying in 5 seconds");
      delay(5000);
    }
  }
}
void publishTelemetry(const char* key, const char* value) {
  String telemetryTopic = String("v1/devices/me/telemetry");
  String payload = String("{\"") + key + "\": " + value + "}";

  mqttClient.publish(telemetryTopic.c_str(), payload.c_str());
}

void parseCGNSINF(const String& response, float* location) {
  int startIndex = response.indexOf(',', response.indexOf(',', response.indexOf(',') + 1) + 1) + 1;
  int endIndex = response.indexOf(',', startIndex);
  if (endIndex != -1) {
    String latitudStr = response.substring(startIndex, endIndex);
    location[0] = latitudStr.toFloat();
  }

  startIndex = endIndex + 1;
  endIndex = response.indexOf(',', startIndex);
  if (endIndex != -1) {
    String longitudStr = response.substring(startIndex, endIndex);
    location[1] = longitudStr.toFloat();
  }

  startIndex = endIndex + 1;
  endIndex = response.indexOf(',', startIndex);
  if (endIndex != -1) {
    String alturaStr = response.substring(startIndex, endIndex);
    // Ignorar la altura (no asignar a location[2])
  }

  startIndex = endIndex + 1;
  endIndex = response.indexOf(',', startIndex);
  if (endIndex != -1) {
    String velocidadStr = response.substring(startIndex, endIndex);
    location[2] = velocidadStr.toFloat();
  }
}

char* convertFloatToChar(float latitude) {
  // Crear un objeto String para almacenar la latitud
  String latitudeStr = String(latitude, 6);

  // Obtener el tamaño de la cadena de caracteres resultante
  int strSize = latitudeStr.length() + 1; // +1 para el carácter nulo '\0'

  // Crear un arreglo de caracteres para almacenar la latitud
  char* latitudeChar = new char[strSize];

  // Copiar la latitud al arreglo de caracteres
  latitudeStr.toCharArray(latitudeChar, strSize);

  // Retornar el arreglo de caracteres
  return latitudeChar;
}
